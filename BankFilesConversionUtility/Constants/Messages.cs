﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankFilesConversionUtility.Constants
{
    public static class Messages
    {
        public const string FileDoesntExist = "File Doesn't Exist";
        public const string MakeSureFileExists = "Input file {0} does not exist. Please make sure the file exists and try again.";
        public const string FolderDoesntExist = "Folder Doesn't Exist";
        public const string MakeSureFolderExists = "Output directory {0} does not exist. Please create it and try again.";
        public const string IncorrectFileType = "Incorrect File Type";
        public const string MakeSureInputFileTypeIsCorrect = "Please make sure your input file {0} ends in {1}";
        public const string PleaseMakeSureOutputFileTypeIsCorrect = "Please make sure your output file {0} ends in {1}";
    }

}
