﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankFilesConversionUtility.Constants
{
    public static class Formatting
    {
        public const string MoreThanOneWhiteSpacesRegexPattern = @"\s{2,}";
        public const string AnyWhiteSpaceRegexPattern = @"\s+";
        public const string PlaceHolder = "*&*";
        public const string TwoSpaces = "  ";
        public const string CsvDelimeter = ",";
        public const string PlaceHolderWithSpaces = TwoSpaces + PlaceHolder + TwoSpaces;
        public const string InputDateTimeFormat = "yyyyMMdd";
        public const string OutputDateTimeFormat = "dd/MM/yyyy";
        public const string InputFileExtension = ".brr";
        public const string OutputFileExtension = ".csv";
        public const int FileExtensionLength = 4;
    }
}
