﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Text.RegularExpressions;
using System.Globalization;
using MessageBox = System.Windows.MessageBox;
using Path = System.IO.Path;
using Constants = BankFilesConversionUtility.Constants.Formatting;
using Microsoft.Win32;
using BankFilesConversionUtility.Constants;

namespace BankFilesConversionUtility
{
    /// <summary>
    /// Logic for creating headings:-
    /// Headings can be on two lines, eg:
    ///      CUSTOMER             PAYMENT       ERR CORR    PAYMENT AMOUNT     PAYMENT  TRANSACTION REF NBR/   PAYMENT   PAYMENT   SETTLEMENT     
    ///   REFERENCE NUMBER    INSTRUCTION TYPE   REASON*                        TYPE**    ORIGINAL REF NBR      DATE                  DATE   
    ///   
    /// 1. Split the first row and second row into a List<string> with white space as the separator.
    /// 2. If the first row contains more elements than the second row, find the difference in the number of elements, this difference is the number of gaps, which would be two in the example above
    /// 3. Iterate from 0 to numberOfGaps, for each iteration:
    ///     a. Find the largest gap in the second row and replace it with a placeHolder
    /// 4. Once again, split the second row into a List<string> with white space as the separator
    /// 5. The second row List<string> now contains the same number of elements as the first row (gaps were replaced with the placeHolder string)
    /// 6. Replace all placeHolder strings in the second row with blank strings
    /// 7. Create a new List<string> which is the first row and second row corresponding elements combined
    /// 8. Convert this new List<string> into a csv formatted string
    /// 

    //TODO:
    //1.      DONE: Change the date format to be dd/mm/yyyy
    //2.      DONE: Use error control (non existing files etc), check .brr and .csv file extensions
    //3.      DONE: Extract headings from csv
    //4.      Have a settings tab where you input the start/end lines
    //5.    Use a file picker
    //6.    Fix the GUI (change window name, change window size, change fonts etc)
    //7.    Separate business logic from GUI (MVVM)
    //8.    DONE: Put constants into their own class
    //9.    Create classes for formatting/extracting etc
    //10.   Fix bug where gaps are consecutive
    //11.   try catch
    //12.   brr content format check

    /// </summary>
    public partial class MainWindow : Window
    {
        readonly CultureInfo EnUS = new CultureInfo("en-US");
        string _outputFilePath = "";

        public MainWindow()
        {
            InitializeComponent();
        }

        private void ConvertButton_Click(object sender, RoutedEventArgs e)
        {
            _outputFilePath = Path.Combine(outputFileFolder.Text, outputFileName.Text);

            if (!AreInputsValid())
            {
                return;
            }
            List<string> inputContent = File.ReadAllLines(inputFile.Text).ToList();
            string columnHeadingsAsCsv = GetColumnHeadingsAsCsv(inputContent);
            List<string> outputContent = new List<string> { columnHeadingsAsCsv };
            const int StartLine = 7;
            const int LinesBeforeEndOfFile = 18;

            int lastLine = inputContent.Count - LinesBeforeEndOfFile;
            for (int i = StartLine; i < lastLine; i++)
            {
                string outputRow = Regex.Replace(inputContent[i], Formatting.AnyWhiteSpaceRegexPattern, Formatting.CsvDelimeter);
                string outputRowWithLastCommaRemoved = outputRow.Remove(outputRow.Length - 1);
                outputContent.Add(outputRowWithLastCommaRemoved);
            }

            outputContent = FormatDateStrings(outputContent);
            File.WriteAllLines(_outputFilePath, outputContent);
        }

        private bool AreInputsValid()
        {
            if (!File.Exists(inputFile.Text))
            {
                ShowMessageBox(Messages.FileDoesntExist, String.Format(Messages.MakeSureFileExists, inputFile.Text));
                return false;
            }
            string outputFileDir = Path.GetDirectoryName(_outputFilePath);
            if (!Directory.Exists(outputFileDir))
            {
                ShowMessageBox(Messages.FolderDoesntExist, String.Format(Messages.MakeSureFolderExists, outputFileDir));
                return false;
            }

            string lastFourCharsOfInputFile = inputFile.Text.Substring(inputFile.Text.Length - Formatting.FileExtensionLength, Formatting.FileExtensionLength);
            if (lastFourCharsOfInputFile.ToLower() != Formatting.InputFileExtension)
            {
                ShowMessageBox(Messages.IncorrectFileType, String.Format(Messages.MakeSureInputFileTypeIsCorrect, inputFile.Text, Formatting.InputFileExtension));
                return false;
            }
            string lastFourCharsOfOutputFile = _outputFilePath.Substring(_outputFilePath.Length - Formatting.FileExtensionLength, Formatting.FileExtensionLength);
            if (lastFourCharsOfOutputFile.ToLower() != Formatting.OutputFileExtension)
            {
                ShowMessageBox(Messages.IncorrectFileType, String.Format(Messages.PleaseMakeSureOutputFileTypeIsCorrect, _outputFilePath, Formatting.OutputFileExtension));
                return false;
            }
            return true;
        }

        private void ShowMessageBox(string heading, string message)
        {
            MessageBox.Show(message,
                heading,
                MessageBoxButton.OK,
                MessageBoxImage.Exclamation);
        }

        private List<string> FormatDateStrings(List<string> outputContent)
        {
            List<int> indicesOfColumnsWithDateTimeStrings = GetColumnIndicesContainingDateTimeStrings(outputContent);
            for (int i = 1; i < outputContent.Count; i++)
            {
                List<string> stringSplitByCommas = outputContent[i].Split(',').ToList();
                foreach (int index in indicesOfColumnsWithDateTimeStrings)
                {
                    string dateTimeFormatted = DateTime.ParseExact(stringSplitByCommas[index], Formatting.InputDateTimeFormat, EnUS, DateTimeStyles.None).ToString(Formatting.OutputDateTimeFormat);
                    stringSplitByCommas[index] = dateTimeFormatted;
                }
                outputContent[i] = stringSplitByCommas.Aggregate((j, k) => j + Formatting.CsvDelimeter + k);
            }

            return outputContent;
        }

        private List<int> GetColumnIndicesContainingDateTimeStrings(List<string> outputContent)
        {
            List<string> stringSplitByCommas = outputContent[1].Split(',').ToList();
            List<int> indicesOfColumnsWithDateTimeStrings = new List<int>();
            for (int i = 0; i < stringSplitByCommas.Count; i++)
            {
                string str = stringSplitByCommas[i];
                DateTime dateValue;
                if (DateTime.TryParseExact(str, Formatting.InputDateTimeFormat, EnUS, DateTimeStyles.None, out dateValue))
                {
                    indicesOfColumnsWithDateTimeStrings.Add(i);
                }
            }
            return indicesOfColumnsWithDateTimeStrings;
        }

        private string GetColumnHeadingsAsCsv(List<string> inputContent)
        {
            List<string> columnHeadings = GetColumnHeadings(inputContent);

            string colNamesAsCsv = "";
            foreach (string column in columnHeadings)
            {
                colNamesAsCsv += column + Formatting.CsvDelimeter;
            }
            colNamesAsCsv = colNamesAsCsv.Remove(colNamesAsCsv.Length - 1);
            return colNamesAsCsv;
        }

        private List<string> GetColumnHeadings(List<string> inputContent)
        {
            const int firstHeadingsRowNumber = 4;
            const int secondHeadingsRowNumber = 5;

            string firstHeadingsRowStr = inputContent[firstHeadingsRowNumber].Trim();
            string secondHeadingsRowStr = inputContent[secondHeadingsRowNumber].Trim();

            List<string> firstHeadingsRow = Regex.Split(firstHeadingsRowStr, Formatting.MoreThanOneWhiteSpacesRegexPattern).ToList();
            List<string> secondHeadingsRow = Regex.Split(secondHeadingsRowStr, Formatting.MoreThanOneWhiteSpacesRegexPattern).ToList();

            if (firstHeadingsRow.Count > secondHeadingsRow.Count)
            {
                string secondHeadingsRowStrWithGapsReplacedWithPlaceHolders = ReplaceGapsWithPlaceHolderString(secondHeadingsRowStr, firstHeadingsRow.Count, secondHeadingsRow.Count);
                List<string> secondHeadingsRowWithGapsReplacedWithPlaceHolders = Regex.Split(secondHeadingsRowStrWithGapsReplacedWithPlaceHolders, Formatting.MoreThanOneWhiteSpacesRegexPattern).ToList();
                List<string> secondHeadingsRowWithPlaceHoldersReplacedWithEmptyStrings = secondHeadingsRowWithGapsReplacedWithPlaceHolders.Select(s => s == Formatting.PlaceHolder ? "" : s).ToList();
                secondHeadingsRow = secondHeadingsRowWithPlaceHoldersReplacedWithEmptyStrings;
            }
            List<string> columnHeadings = firstHeadingsRow.Zip(secondHeadingsRow, (a, b) => a + " " + b).ToList();
            return columnHeadings;
        }

        private string ReplaceGapsWithPlaceHolderString(string secondHeadingsRowStr, int firstHeadingsRowCount, int secondHeadingsRowCount)
        {
            int numOfGaps = firstHeadingsRowCount - secondHeadingsRowCount;
            for (int i = 0; i < numOfGaps; i++)
            {
                string stringWithMostWhiteSpaces = GetStringWithMostWhiteSpaces(secondHeadingsRowStr);
                secondHeadingsRowStr = secondHeadingsRowStr.Replace(stringWithMostWhiteSpaces, Formatting.PlaceHolderWithSpaces);
            }
            return secondHeadingsRowStr;
        }

        private string GetStringWithMostWhiteSpaces(string secondHeadingsRowStr)
        {
            return Regex.Matches(secondHeadingsRowStr, Formatting.MoreThanOneWhiteSpacesRegexPattern)
                        .OfType<Match>()
                        .Select(match => match.Value)
                        .ToList().Aggregate("", (max, cur) => max.Length > cur.Length ? max : cur);
        }

        private void InputFilePickerButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();

            dlg.DefaultExt = ".png";
            dlg.Filter = "BRR Files (*.brr)|*.brr";

            bool? result = dlg.ShowDialog();

            if (result.HasValue && result.Value)
            {
                inputFile.Text = dlg.FileName;
            }
        }

        private void OutputFilePickerButon_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            System.Windows.Forms.DialogResult result = dialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                outputFileFolder.Text = dialog.SelectedPath;
            }
        }
    }
}
